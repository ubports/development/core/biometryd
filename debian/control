Source: biometryd
Priority: optional
Maintainer: UBports Developers <devs@ubports.com>
Build-Depends: cmake,
               cmake-extras,
               debhelper-compat (= 12),
               doxygen,
               google-mock,
               graphviz,
               libapparmor-dev,
               libboost-filesystem-dev,
               libboost-program-options-dev,
               libboost-system-dev,
               libboost-test-dev,
               libdbus-cpp-dev (>= 4.0.0),
               libdbus-1-dev,
               libelf-dev,
               libgtest-dev,
               libhybris-common-dev | hello,
               libprocess-cpp-dev,
               libsqlite3-dev,
               lsb-release,
               nlohmann-json3-dev,
               pkg-config,
               qml-module-qttest,
               qtbase5-dev,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               systemd-dev,
Standards-Version: 3.9.5
Section: libs
Homepage: https://gitlab.com/ubports/development/core/biometryd
Vcs-Git: https://gitlab.com/ubports/development/core/biometryd.git
Vcs-Browser: https://gitlab.com/ubports/development/core/biometryd

Package: libbiometry1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: libbiometryd1 (<< 0.0.3~),
Replaces: libiometryd1 (<< 0.0.3~),
Recommends: biometryd-bin,
Description: biometryd mediates/multiplexes to biometric devices - runtime library
 biometryd mediates and multiplexes access to biometric devices present
 on the system, enabling applications and system components to leverage
 them for identification and verification of users.
 .
 This package includes the biometryd runtime libraries.

Package: libbiometry-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: libbiometry1 (= ${binary:Version}),
         ${misc:Depends},
Breaks: libbiometryd-dev (<< 0.0.3~),
Replaces: libiometryd-dev (<< 0.0.3~),
Description: biometryd mediates/multiplexes to biometric devices - development headers
 biometryd mediates and multiplexes access to biometric devices present
 on the system, enabling applications and system components to leverage
 them for identification and verification of users.
 .
 This package includes all the development headers and libraries for
 biometryd.

Package: biometryd-bin
Section: devel
Architecture: any
Multi-Arch: foreign
Depends: libbiometry1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: biometryd mediates/multiplexes to biometric devices - daemon/helper binaries
 biometryd mediates and multiplexes access to biometric devices present
 on the system, enabling applications and system components to leverage
 them for identification and verification of users.
 .
 Daemon and helper binaries to be used by services.

Package: qml-module-biometryd
Section: devel
Architecture: any
Depends: libbiometry1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: biometryd mediates/multiplexes to biometric devices - QML bindings
 biometryd mediates and multiplexes access to biometric devices present
 on the system, enabling applications and system components to leverage
 them for identification and verification of users.
 .
 This package contains the qtdeclarative bindings for biometryd.
